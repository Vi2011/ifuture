//
//  ForgotPasswordViewController.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    var presenter : ForgotPasswordPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}

extension ForgotPasswordViewController : ForgotPasswordViewProtocol {
    
}
