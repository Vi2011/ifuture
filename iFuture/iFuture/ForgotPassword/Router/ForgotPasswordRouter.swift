//
//  ForgotPasswordRouter.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordRouter : ForgotPasswordRouterProtocol {
    
    static var rootViewController: UIViewController?
    
    static var mainStoryboard : UIStoryboard {
        return UIStoryboard(name: "ForgotPassword", bundle: nil)
    }
    
    static func assemblyModule() -> UIViewController {
        let forgotPasswordViewController = ForgotPasswordRouter.mainStoryboard.instantiateViewController(withIdentifier: "RegisterNav")
        if let view = forgotPasswordViewController.childViewControllers.first as? ForgotPasswordViewController {
            let presenter : ForgotPasswordPresenterProtocol & ForgotPasswordInteractorCallbackProtocol = ForgotPasswordPresenter()
            let interactor : ForgotPasswordInteractorProtocol = ForgotPasswordInteractor()
            let rooter : ForgotPasswordRouter = ForgotPasswordRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = rooter
            interactor.presenter = presenter
            rootViewController = forgotPasswordViewController
            
            return forgotPasswordViewController
        }
        return UIViewController()
    }
    
    func popToRootViewController() {
        ForgotPasswordRouter.rootViewController?.navigationController?.popViewController(animated: true)
    }
    
    func presentToViewController(from view1: ForgotPasswordViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.present(view2, animated: true, completion: nil)
        }
    }
    
    func pushToViewController(from view1: ForgotPasswordViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.pushViewController(view2, animated: true)
        }
    }
}
