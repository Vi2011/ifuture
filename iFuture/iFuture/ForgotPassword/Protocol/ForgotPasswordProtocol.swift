//
//  ForgotPasswordProtocol.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

protocol ForgotPasswordPresenterProtocol: class {
    var view : ForgotPasswordViewProtocol? {get set}
    var interactor : ForgotPasswordInteractorProtocol? {get set}
    var router : ForgotPasswordRouterProtocol? {get set}
    
}

protocol ForgotPasswordInteractorProtocol: class {
    var presenter : ForgotPasswordInteractorCallbackProtocol? {get set}
    
}

protocol ForgotPasswordViewProtocol: class {
    var presenter : ForgotPasswordPresenterProtocol? {get set}
}

protocol ForgotPasswordInteractorCallbackProtocol: class {
    
}

protocol ForgotPasswordRouterProtocol: class {
    func pushToViewController(from view1: ForgotPasswordViewProtocol, to view2: UIViewController, animated: Bool)
    func presentToViewController(from view1: ForgotPasswordViewProtocol, to view2: UIViewController, animated: Bool)
}
