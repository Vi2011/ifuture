//
//  AccountInfoProtocol.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

protocol AccountInfoPresenterProtocol: class {
    var view : AccountInfoViewProtocol? {get set}
    var interactor : AccountInfoInteractorProtocol? {get set}
    var router : AccountInfoRouterProtocol? {get set}
    
}

protocol AccountInfoInteractorProtocol: class {
    var presenter : AccountInfoInteractorCallbackProtocol? {get set}
    
}

protocol AccountInfoViewProtocol: class {
    var presenter : AccountInfoPresenterProtocol? {get set}
}

protocol AccountInfoInteractorCallbackProtocol: class {
    
}

protocol AccountInfoRouterProtocol: class {
    func pushToViewController(from view1: AccountInfoViewProtocol, to view2: UIViewController, animated: Bool)
    func presentToViewController(from view1: AccountInfoViewProtocol, to view2: UIViewController, animated: Bool)
}
