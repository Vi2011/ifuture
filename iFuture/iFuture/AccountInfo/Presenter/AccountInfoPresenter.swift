//
//  AccountInfoPresenter.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class AccountInfoPresenter : AccountInfoPresenterProtocol {
    
    weak var view: AccountInfoViewProtocol?
    var interactor: AccountInfoInteractorProtocol?
    var router: AccountInfoRouterProtocol?
}

extension AccountInfoPresenter : AccountInfoInteractorCallbackProtocol {
    
}
