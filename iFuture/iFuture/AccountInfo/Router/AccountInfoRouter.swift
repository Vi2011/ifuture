//
//  AccountInfoRouter.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class AccountInfoRouter : AccountInfoRouterProtocol {
    
    static var rootViewController: UIViewController?
    
    static var mainStoryboard : UIStoryboard {
        return UIStoryboard(name: "AccountInfo", bundle: nil)
    }
    
    static func assemblyModule() -> UIViewController {
        let accountInfoViewController = AccountInfoRouter.mainStoryboard.instantiateViewController(withIdentifier: "AccountInfoNav")
        if let view = accountInfoViewController.childViewControllers.first as? AccountInfoViewController {
            let presenter : AccountInfoPresenterProtocol & AccountInfoInteractorCallbackProtocol = AccountInfoPresenter()
            let interactor : AccountInfoInteractorProtocol = AccountInfoInteractor()
            let rooter : AccountInfoRouter = AccountInfoRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = rooter
            interactor.presenter = presenter
            rootViewController = accountInfoViewController
            
            return accountInfoViewController
        }
        return UIViewController()
    }
    
    func popToRootViewController() {
        AccountRouter.rootViewController?.navigationController?.popViewController(animated: true)
    }
    
    func presentToViewController(from view1: AccountInfoViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.present(view2, animated: true, completion: nil)
        }
    }
    
    func pushToViewController(from view1: AccountInfoViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.pushViewController(view2, animated: true)
        }
    }
}
