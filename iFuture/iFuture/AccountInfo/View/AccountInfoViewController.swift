//
//  AccountInfoViewController.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import UIKit

class AccountInfoViewController: UIViewController {

    var presenter : AccountInfoPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}

extension AccountInfoViewController : AccountInfoViewProtocol {
    
}
