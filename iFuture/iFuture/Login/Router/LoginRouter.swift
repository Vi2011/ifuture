//
//  LoginRouter.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class LoginRouter : LoginRouterProtocol {
    
    static var rootViewController: UIViewController?
    
    static var mainStoryboard : UIStoryboard {
        return UIStoryboard(name: "Login", bundle: nil)
    }
    
    static func assemblyModule() -> UIViewController {
        let loginViewController = LoginRouter.mainStoryboard.instantiateViewController(withIdentifier: "LoginNav")
        if let view = loginViewController.childViewControllers.first as? LoginViewController {
            let presenter : LoginPresenterProtocol & LoginInteractorCallbackProtocol = LoginPresenter()
            let interactor : LoginInteractorProtocol = LoginInteractor()
            let rooter : LoginRouter = LoginRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = rooter
            interactor.presenter = presenter
            rootViewController = loginViewController
            
            return loginViewController
        }
        return UIViewController()
    }
    
    func popToRootViewController() {
        LoginRouter.rootViewController?.navigationController?.popViewController(animated: true)
    }
    
    func presentToViewController(from view1: LoginViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.present(view2, animated: true, completion: nil)
        }
    }
    
    func pushToViewController(from view1: LoginViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.pushViewController(view2, animated: true)
        }
    }
}
