//
//  LoginViewController.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    //Outlet
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    
    //Var
    var presenter : LoginPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func forgotPasswordBtnTouchdown(_ sender: Any) {
    }
    
    @IBAction func registerBtnTouchDown(_ sender: Any) {
    }
    
    @IBAction func loginBtnTouchDown(_ sender: Any) {
    }
    
}

extension LoginViewController : LoginViewProtocol {
    
}
