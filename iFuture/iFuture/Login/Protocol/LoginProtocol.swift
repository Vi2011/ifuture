//
//  LoginProtocol.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

protocol LoginPresenterProtocol: class {
    var view : LoginViewProtocol? {get set}
    var interactor : LoginInteractorProtocol? {get set}
    var router : LoginRouterProtocol? {get set}
    
}

protocol LoginInteractorProtocol: class {
    var presenter : LoginInteractorCallbackProtocol? {get set}
    
}

protocol LoginViewProtocol: class {
    var presenter : LoginPresenterProtocol? {get set}
}

protocol LoginInteractorCallbackProtocol: class {
    
}

protocol LoginRouterProtocol: class {
    func pushToViewController(from view1: LoginViewProtocol, to view2: UIViewController, animated: Bool)
    func presentToViewController(from view1: LoginViewProtocol, to view2: UIViewController, animated: Bool)
}
