//
//  AccountPresenter.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class AccountPresenter : AccountPresenterProtocol {
    
    weak var view: AccountViewProtocol?
    var interactor: AccountInteractorProtocol?
    var router: AccountRouterProtocol?
}

extension AccountPresenter : AccountInteractorCallbackProtocol {
    
}
