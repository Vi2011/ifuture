//
//  AccountProtocol.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

protocol AccountPresenterProtocol: class {
    var view : AccountViewProtocol? {get set}
    var interactor : AccountInteractorProtocol? {get set}
    var router : AccountRouterProtocol? {get set}
    
}

protocol AccountInteractorProtocol: class {
    var presenter : AccountInteractorCallbackProtocol? {get set}
    
}

protocol AccountViewProtocol: class {
    var presenter : AccountPresenterProtocol? {get set}
}

protocol AccountInteractorCallbackProtocol: class {
    
}

protocol AccountRouterProtocol: class {
    func pushToViewController(from view1: AccountViewProtocol, to view2: UIViewController, animated: Bool)
    func presentToViewController(from view1: AccountViewProtocol, to view2: UIViewController, animated: Bool)
}
