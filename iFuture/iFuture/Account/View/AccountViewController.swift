//
//  AccountViewController.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    var presenter : AccountPresenterProtocol?
    @IBOutlet weak var accountNameLb: UILabel!
    @IBOutlet weak var scoreLb: UILabel!
    @IBOutlet weak var levelImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func accountImgBtnTouchDown(_ sender: Any) {
    }
    
    @IBAction func accountInfoBtnTouchDown(_ sender: Any) {
    }
    
    @IBAction func changePasswordBtnTouchDown(_ sender: Any) {
    }
    
    @IBAction func courseJoinedBtnTouchDown(_ sender: Any) {
    }
    
}

extension AccountViewController : AccountViewProtocol {
    
}
