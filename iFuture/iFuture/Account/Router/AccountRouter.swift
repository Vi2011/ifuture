//
//  AccountRouter.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class AccountRouter : AccountRouterProtocol {
    
    static var rootViewController: UIViewController?
    
    static var mainStoryboard : UIStoryboard {
        return UIStoryboard(name: "Account", bundle: nil)
    }
    
    static func assemblyModule() -> UIViewController {
        let accountViewController = AccountRouter.mainStoryboard.instantiateViewController(withIdentifier: "AccountNav")
        if let view = accountViewController.childViewControllers.first as? AccountViewController {
            let presenter : AccountPresenterProtocol & AccountInteractorCallbackProtocol = AccountPresenter()
            let interactor : AccountInteractorProtocol = AccountInteractor()
            let rooter : AccountRouter = AccountRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = rooter
            interactor.presenter = presenter
            rootViewController = accountViewController
            
            return accountViewController
        }
        return UIViewController()
    }
    
    func popToRootViewController() {
        AccountRouter.rootViewController?.navigationController?.popViewController(animated: true)
    }
    
    func presentToViewController(from view1: AccountViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.present(view2, animated: true, completion: nil)
        }
    }
    
    func pushToViewController(from view1: AccountViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.pushViewController(view2, animated: true)
        }
    }
}
