//
//  RegisterViewController.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    var presenter : RegisterPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func registerBtnTouchDown(_ sender: Any) {
    }
    
}

extension RegisterViewController : RegisterViewProtocol {
    
}
