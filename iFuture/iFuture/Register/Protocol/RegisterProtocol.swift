//
//  RegisterProtocol.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

protocol RegisterPresenterProtocol: class {
    var view : RegisterViewProtocol? {get set}
    var interactor : RegisterInteractorProtocol? {get set}
    var router : RegisterRouterProtocol? {get set}
    
}

protocol RegisterInteractorProtocol: class {
    var presenter : RegisterInteractorCallbackProtocol? {get set}
    
}

protocol RegisterViewProtocol: class {
    var presenter : RegisterPresenterProtocol? {get set}
}

protocol RegisterInteractorCallbackProtocol: class {
    
}

protocol RegisterRouterProtocol: class {
    func pushToViewController(from view1: RegisterViewProtocol, to view2: UIViewController, animated: Bool)
    func presentToViewController(from view1: RegisterViewProtocol, to view2: UIViewController, animated: Bool)
}
