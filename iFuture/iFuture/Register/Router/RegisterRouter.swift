//
//  RegisterRouter.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class RegisterRouter : RegisterRouterProtocol {
    
    static var rootViewController: UIViewController?
    
    static var mainStoryboard : UIStoryboard {
        return UIStoryboard(name: "Register", bundle: nil)
    }
    
    static func assemblyModule() -> UIViewController {
        let registerViewController = RegisterRouter.mainStoryboard.instantiateViewController(withIdentifier: "RegisterNav")
        if let view = registerViewController.childViewControllers.first as? RegisterViewController {
            let presenter : RegisterPresenterProtocol & RegisterInteractorCallbackProtocol = RegisterPresenter()
            let interactor : RegisterInteractorProtocol = RegisterInteractor()
            let rooter : RegisterRouter = RegisterRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = rooter
            interactor.presenter = presenter
            rootViewController = registerViewController
            
            return registerViewController
        }
        return UIViewController()
    }
    
    func popToRootViewController() {
        RegisterRouter.rootViewController?.navigationController?.popViewController(animated: true)
    }
    
    func presentToViewController(from view1: RegisterViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.present(view2, animated: true, completion: nil)
        }
    }
    
    func pushToViewController(from view1: RegisterViewProtocol, to view2: UIViewController, animated: Bool) {
        if let sourceView = view1 as? UIViewController {
            sourceView.navigationController?.pushViewController(view2, animated: true)
        }
    }
}
