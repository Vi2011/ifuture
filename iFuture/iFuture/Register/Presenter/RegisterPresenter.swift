//
//  RegisterPresenter.swift
//  iFuture
//
//  Created by Ms Vi Nguyen Tuong Vi on 7/9/30 H.
//  Copyright © 30 Heisei Ms Vi Nguyen Tuong Vi. All rights reserved.
//

import Foundation
import UIKit

class RegisterPresenter : RegisterPresenterProtocol {
    
    weak var view: RegisterViewProtocol?
    var interactor: RegisterInteractorProtocol?
    var router: RegisterRouterProtocol?
}

extension RegisterPresenter : RegisterInteractorCallbackProtocol {
    
}
